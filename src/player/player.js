$.fn.extend({
    serial_fade: function(o) {
        if(!o.speed || o.speed == undefined || o.speed == null) { o.speed = 'slow'; }
        if(!o.fade || o.fade == undefined || o.fade == null)    { o.fade = 'in'; }
        if(!o.index || o.index == undefined || o.index == null) { o.index = 0; }
        var s = this.selector;
        if(o.fade.toLowerCase() == 'in') {
            return this.eq(o.index).fadeIn(o.speed, function() {
                o.index++;
                if($(s).eq(o.index).length > 0) {
                    $(s).serial_fade({speed:o.speed,fade:o.fade,index:o.index});
                }
            });
        } else {
            return this.eq(o.index).fadeOut(o.speed, function() {
                o.index++;
                if($(s).eq(o.index).length > 0) {
                    $(s).serial_fade({speed:o.speed,fade:o.fade,index:o.index});
                }
            });
        }
    }
});

var BACKGROUND_IMAGE_PATH = "assets/images/backgrounds/";
var COSTUME_PATH = "assets/images/sprites/";

var Player = {
    Views: {},
    Models: {},
    Collections: {},
    Factories: {},

    initialize: function() {
      $("#play").on("click", function() {
        var script = localStorage.program;
        var temp_lines = script.split("\n");
        var lines = new Array();
        
        while(temp_lines.length > 0) {
          temp_line = temp_lines.shift();
          if(temp_line != "") {
            lines.push(temp_line);
          }
        }

        var programData = new Player.Models.BlockInstruction({lines: lines});
        var programView = new Player.Views.Player({model: programData});
        $("#player-field").html("");
        $("#player-field").html(programView.render().$el);
      });
    }
};

Player.Models.Instruction = Backbone.Model.extend({
  
  defaults: {
    name: "default", 
    hasParam: false,
    isBlock: false,
    value: 0,
    sub_instructs: null,
  },  

  initialize: function(options) {
    this.set('sub_instructs', new Array());
    var tokens = options.lines[0].split(" ");
    this.set('name', tokens[0]);
    if(tokens.length > 1) {
      this.set('hasParam', true);
      this.set('value', tokens[1]);
    }
  }
});

Player.Models.BlockInstruction = Player.Models.Instruction.extend({
  initialize: function(options) {
    this.set('sub_instructs', new Array());

    var instructs = options.lines;
    instructs.pop();

    var first_line = instructs.shift();
    this.set_general_values(first_line);

    while(instructs.length > 0) {
      var line = instructs.shift();

      if(line.indexOf("repeat") > -1) {
        var temp_instructs = [line];
        var temp_begin = ["repeat"];

        while(temp_begin.length > 0) {
          var new_line = instructs.shift();
          if(new_line.indexOf("repeat") > -1) {
            temp_begin.push("repeat");
          }
          if(new_line.indexOf("end") > -1) {
            temp_begin.pop();
          }
          temp_instructs.push(new_line);
        }

        var temp = this.get('sub_instructs');
        temp.push(new Player.Models.BlockInstruction({lines: temp_instructs}));
        this.set('sub_instructs', temp);
      }
      else {
        var temp = this.get('sub_instructs');
        temp.push(new Player.Models.Instruction({lines: [line]}));
        this.set('sub_instructs', temp);
      }
    }
  },

  set_general_values: function(line) {
    var tokens = line.split(" ");
    this.set('name', tokens[0]);
    this.set('hasParam', true);
    this.set('isBlock', true);
    this.set('value', tokens[1]);
  },
});

Player.Views.Player = Backbone.View.extend({
  parent_div: $("#player-field"),

  initialize: function(options) {
    self.model = options.model;

    this.backgrounds = new Editor.Collections.Backgrounds();
    this.backgrounds.fetch({async:false});

    this.costumes = new Editor.Collections.Costumes();
    this.costumes.fetch({async:false});
    this.current_background = this.backgrounds.first();
    this.parent_div.css('background-image', 'url(' + BACKGROUND_IMAGE_PATH + this.current_background.get('image') + ')');

    this.current_costume = this.costumes.first();
    this.character = $("<img id='character'>");
    this.character.attr('src', COSTUME_PATH+this.current_costume.get('image'));
    this.character.css('position', 'absolute');
    this.character.css('top', '0px');
    this.character.css('left', '0px');
    this.character.hide();
    this.$el.append(this.character);
    this.actionQueue = $({});
  },

  render: function() {
    this.playBlockAction(this.model);
    console.log(this.actionQueue);
    this.actionQueue.dequeue("action");
    return this;
  },

  playBlockAction: function(current_instruciton) {
    var time = parseInt(current_instruciton.get('value'));
    var name = current_instruciton.get('name');
    var that = this;

    if(name == "repeat") {
      var i;
      for(i=0; i<time; i++) {
        $.each(current_instruciton.get('sub_instructs'), function(i, instruction) {
          if(instruction.get("isBlock")) {
            that.playBlockAction(instruction);
          }
          else {
            that.playSingleAction(instruction); 
          }
        });
      }
    }
  },

  playSingleAction: function(current_instruciton) {
    var name = current_instruciton.get('name');
    var that = this;
    if(name == "show") {
      this.actionQueue.queue('action', function(next) {
        that.character.show();
        console.log("show");
        next();
      });
    } else if(name == "hide") {
      this.actionQueue.queue('action', function(next) {
        that.character.hide();
        console.log("hide");
        next();
      });
    } else if(name == "setX") {
      this.actionQueue.queue('action', function(next) {
        that.character.css("left", current_instruciton.get('value')+"px"); 
        console.log("setX");
        next();
      });
    } else if(name == "setY") {
      this.actionQueue.queue('action', function(next) {
        that.character.css("top", current_instruciton.get('value')+"px");
        console.log("setY");
        next();
      });
    } else if(name == "nextCostume") {
      this.actionQueue.queue('action', function(next) {
        var temp_index = that.costumes.indexOf(that.current_costume) + 1;
        temp_index = temp_index % that.costumes.length;
        that.current_costume = that.costumes.at(temp_index);
        that.$el.find("#character").attr("src", COSTUME_PATH+that.current_costume.get('image'));
        console.log("nextCostume");
        next();
      });
    } else if(name == "nextBackground") {
      this.actionQueue.queue('action', function(next) {
        var temp_index = that.backgrounds.indexOf(that.current_background) + 1;
        temp_index = temp_index % that.backgrounds.length;
        that.current_background = that.backgrounds.at(temp_index);
        that.parent_div.css('background-image', 'url(' + BACKGROUND_IMAGE_PATH + that.current_background.get('image') + ')');
        console.log("currentBackground");
      });
    } else if(name == "move") {
      this.actionQueue.queue('action', function(next) {
        var steps =  parseInt(current_instruciton.get('value'));
        console.log(steps);
        that.character.animate({ 
          left: "+="+steps+"px",
        }, 100 );
        console.log("move");
        next();
      });
    } else {
      console.log("nothing");
    }
  }
});