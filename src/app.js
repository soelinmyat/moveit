// data to bootstrap
var instructions = [
    {
        id: 1, 
        name: "move", 
        hasParam: true,
        unit: "steps"
    },
/*    {
        id: 2, 
        name: "rotate", 
        hasParam: true,
        unit: "degrees"
    }, 
*/
    {
        id: 3, 
        name: "repeat", 
        hasParam: true,
        isBlock: true
    },
    {
        id: 4, 
        name: "show"
    },
    {
        id: 5, 
        name: "hide"
    },
    {
        id: 6, 
        name: "setX",
        hasParam: true,
        unit: "px"
    },
    {
        id: 7, 
        name: "setY",
        hasParam: true,
        unit: "px"
    },
    {
        id: 8,
        name: "nextCostume",
        hasParam: false,
        unit: ""
    },
    {
        id: 9,
        name: "nextBackground",
        hasParam: false,
        unit: ""
    },
/*
    {
        id: 6,
        name: "switch consume to",
        hasParam: true
    },
    {
        id: 7,
        name: "switch background to",
        hasParam: true
    }
*/
];

var backgrounds = [
  { id: 1, image: "bedroom1.gif" },
  { id: 2, image: "bedroom2.gif" },
  { id: 3, image: "atom-playground.jpg.gif" }
];

var costumes = [
    {id: 1, image: "dog1-a.png"},
    {id: 2, image: "dog1-b.png"},
];

var App = {
    initialize: function(instructions, backgrounds, costumes) {
        this.bootstrap_data(instructions, backgrounds, costumes);
    },

    bootstrap_data: function(instructions, backgrounds, costumes) {

        // save instructions into local storage
        var instructions_collection = new Editor.Collections.AvailableInstructions();
        $.each(instructions, function(i, instruction) {
            instructions_collection.create(instruction);
        });

        // save backgrounds into local storage
        var backgrounds_collection = new Editor.Collections.Backgrounds();
        $.each(backgrounds, function(i, background) {
            backgrounds_collection.create(background);
        });

        // save consumes into local storage
        var costumes_collection = new Editor.Collections.Costumes();
        $.each(costumes, function(i, background) {
            costumes_collection.create(background);
        });
    }
};

$(function() {
    App.initialize(instructions, backgrounds, costumes);
    Editor.initialize();
    Player.initialize();
});