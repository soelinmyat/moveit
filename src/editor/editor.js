var Editor = {
    Views: {},
    Models: {},
    Collections: {},
    Factories: {},

    initialize: function(instructions) {
      var editor_model = Editor.Factories.InstructionModelFactory.create_new('repeat', 1);
      editor_model.set("value", 1);
      var editor_view = new Editor.Views.BlockInstructionView({
        model: editor_model,
        parent: null,
        enable_save: true
      });
      $("#editor").html(editor_view.render().el);
    }
};