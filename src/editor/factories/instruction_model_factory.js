Editor.Factories.InstructionModelFactory = {
  create_new: function(name, serial) {
    var new_instruction;
    if(name == "move") {
      new_instruction = new Editor.Models.MoveInstruction();
      new_instruction.set("serial", serial);
    }
    else if(name == "show") {
      new_instruction = new Editor.Models.ShowInstruction();
      new_instruction.set("serial", serial);
    }
    else if(name == "hide") {
      new_instruction = new Editor.Models.HideInstruction();
      new_instruction.set("serial", serial);
    }
    else if(name == "setX") {
      new_instruction = new Editor.Models.SetXInstruction();
      new_instruction.set("serial", serial);
    }
    else if(name == "setY") {
      new_instruction = new Editor.Models.SetYInstruction();
      new_instruction.set("serial", serial);
    }
     else if(name == "nextCostume") {
      new_instruction = new Editor.Models.NextCostumeInstruction();
      new_instruction.set("serial", serial);
    }
     else if(name == "nextBackground") {
      new_instruction = new Editor.Models.NextBackgroundInstruction();
      new_instruction.set("serial", serial);
    }
    else {
      new_instruction = new Editor.Models.RepeatInstruction();
      new_instruction.set("serial", serial);
    }
    return new_instruction;
  }
}