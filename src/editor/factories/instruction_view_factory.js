Editor.Factories.InstructionViewFactory = {
  create_new: function(model, parent) {
    var new_instruction_view;
    if(model.get("isBlock")) {
      new_instruction_view = new Editor.Views.BlockInstructionView({model: model, parent: parent});
    }
    else {
      new_instruction_view = new Editor.Views.SingleInstructionView({model: model, parent: parent});
    }
    return new_instruction_view;
  }
}