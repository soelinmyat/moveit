Editor.Models.Instruction = Backbone.Model.extend({
  defaults: {
    name: "default", 
    hasParam: false,
    unit: "",
    isBlock: false,
    value: 0,
    serial: 0,
    enable_save: false
  }
});

Editor.Models.SingleInstruction = Editor.Models.Instruction.extend({

  to_string: function(indent) {
    //indent = typeof indent !== 'undefined' ? indent : "";
    indent = "";

    var str = indent;
    str += this.get("name");

    if(this.get("hasParam")) {
      str += " " + this.get("value");
    }

    str += "\n";

    return str;
  }
});

Editor.Models.BlockInstruction = Editor.Models.Instruction.extend({

  initialize: function(options) {
    this.isBlock = true;
    this.instructions = new Editor.Collections.Instructions();
  },

  to_string: function(indent) {
    //indent = typeof indent !== 'undefined' ? indent : "";
    indent = "";

    var str = indent;

    str += this.get("name");

    if(this.get("hasParam")) {
      str += " " + this.get("value");
    }
    str += " do\n";

    this.instructions.each(function(instruction) {
      str += instruction.to_string(indent+"  ");
    });

     str = str + indent + "end\n";

    return str;
  },

  add_instruction: function(new_model) {
    this.instructions.add(new_model);
  }
});

Editor.Models.MoveInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "move", 
    hasParam: true,
    unit: "steps",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.SetXInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "setX", 
    hasParam: true,
    unit: "px",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.SetYInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "setY", 
    hasParam: true,
    unit: "px",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.NextCostumeInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "nextCostume", 
    hasParam: false,
    unit: "",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.NextBackgroundInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "nextBackground", 
    hasParam: false,
    unit: "",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.ShowInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "show", 
    hasParam: false,
    unit: "",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.HideInstruction = Editor.Models.SingleInstruction.extend({
  defaults: {
    name: "hide", 
    hasParam: false,
    unit: "",
    isBlock: false,
    value: 10,
    enable_save: false
  }
});

Editor.Models.RepeatInstruction = Editor.Models.BlockInstruction.extend({
  defaults: {
    name: "repeat", 
    hasParam: true,
    unit: "times",
    isBlock: true,
    value: 10,
    enable_save: false
  }
});