Editor.Views.InstructionLabelView = Backbone.View.extend({

  initialize: function(options) {
    this.model = options.model;
    this.template = $("#instruction-label").html();
  },

  render: function() {
    tmpl = _.template(this.template);
    this.$el.append(tmpl(this.model.toJSON()));
    return this;
  }

});