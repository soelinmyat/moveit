Editor.Views.BlockInstructionView = Backbone.View.extend(_.extend({}, Backbone.Events, {
  className: "block-instruction",

  initialize: function(options) {
    this.model = options.model;
    this.model.set("id", this.model.cid);

    this.parent = options.parent;
    if(options.enable_save != undefined)
    {
      this.model.set("enable_save", true);
    }

    this.template = $("#instruction-block").html();
    this.setup_view();

    this.add_instruction_view = new Editor.Views.AddInstructionView({parent: this});
    this.on('add_instruction', this.add_instruction_handler, this);
    this.on('up_instruction', this.up_instruction_handler, this);
    this.on('delete_instruction', this.delete_instruction_handler, this);

    $(this.el).find("#add-instruction-"+ this.model.get("id")).on("click", {that: this}, this.show_add_instruction_view);
    $(this.el).find("#delete-instruction-"+ this.model.get("id")).on("click", {that: this}, this.delete_instruction);
    $(this.el).find("#up-instruction-"+ this.model.get("id")).on("click", {that: this}, this.up_instruction);
    $(this.el).find("#save-instruction-"+ this.model.get("id")).on("click", {that: this}, this.save_instruction);
    this.next_instruction_serial = 1;
  },

  setup_view: function() {
    tmpl = _.template(this.template);
    this.$el.append(tmpl(this.model.toJSON()));
  },

  render: function() {
    var that = this;
    that.$el.find("#instruction-div-" + that.model.get("id")).html("");

    that.model.instructions.each(function(instruction) {
      var instructionView = Editor.Factories.InstructionViewFactory.create_new(instruction, that);
      that.$el.find("#instruction-div-" + that.model.get("id")).append(instructionView.render().el);
      that.$el.find("#input-"+ that.model.get("id")).on("change", {that: that}, that.content_changed);
    });

    return this;
  },

  events: {
    "click #add-instruction": "show_add_instruction_view"
  },

  show_add_instruction_view: function(event) {
    event.data.that.add_instruction_view.show();
  },

  add_instruction_handler: function(name) {
    this.model.add_instruction(Editor.Factories.InstructionModelFactory.create_new(name, this.next_instruction_serial));
    this.render();
    this.next_instruction_serial = this.next_instruction_serial + 1;
  },

  up_instruction_handler: function(cid) {
    var temp_model = this.model.instructions.get(cid);
    var index = this.model.instructions.indexOf(temp_model);
    if(index != 0) {
      var above_model = this.model.instructions.at(index-1);
      var temp_serial = above_model.get("serial");
      above_model.set("serial", temp_model.get("serial"));
      temp_model.set("serial", temp_serial);
      this.model.instructions.sort();
      this.render();
    }
  },

  delete_instruction_handler: function(cid) {
    var temp_model = this.model.instructions.get(cid);
    this.model.instructions.remove(temp_model);
    this.render();
  },

  content_changed: function(event) {
    var that = event.data.that;
    var value = $(that.el).find("#input-"+ that.model.get("id")).val();
    that.model.set("value", value);
  },

  up_instruction: function(event) {
    event.data.that.parent.trigger('up_instruction', event.data.that.model.cid);
  },

  delete_instruction: function(event) {
    if(event.data.that.parent != undefined) {
      event.data.that.parent.trigger('delete_instruction', event.data.that.model.cid);
    }
  },

  save_instruction: function(event) {
    var that = event.data.that;
    console.log(that.model.to_string());
    localStorage.program = that.model.to_string();
  }

}));