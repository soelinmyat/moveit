Editor.Views.AddInstructionView = Backbone.View.extend({

  initialize: function(options) {
    var that = this;
    that.parent = options.parent;
    that.template = $("#instruction-label").html();
    that.instructions = new Editor.Collections.AvailableInstructions();
    that.instructions.fetch({success: function(model, response){ that.render(); }});
  },

  render: function() {
    var that = this;
    tmpl = _.template(that.template);
    that.$el.html("");
    that.instructions.each(function(instruction) {
      var instructionView = new Editor.Views.InstructionLabelView({model: instruction});
      that.$el.append(instructionView.render().el);
    });
    return that;
  },

  show: function() {
    var that = this;
    this.$el.find('.instruction-label').removeClass("selected");
    this.$el.dialog({
      modal: true,
      buttons: {
        "Add": function() {
          if(that.$el.find('.selected').length == 0) {
            alert("Choose an instruction to add.");
          }
          else {
            that.parent.trigger('add_instruction', that.$el.find('.selected').data("name"));
            $(this).dialog("close");
          }
        },
        Cancel: function() {
          $(this).dialog("close");
        }
      }
    });
  },

  events: {
    "click .instruction-label": "choose_instruction"
  },

  choose_instruction: function(e)
  {
    this.$el.find('.instruction-label').removeClass("selected");
    $(e.currentTarget).addClass("selected");
  }

});