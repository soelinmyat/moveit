Editor.Views.SingleInstructionView = Backbone.View.extend({
  className: "single-instruction",

  initialize: function(options) {
    this.parent = options.parent;
    this.model = options.model;
    this.model.set("id", this.model.cid);
    this.template = $("#single-instruction").html();
  },

  render: function() {
    tmpl = _.template(this.template);
    this.$el.html(tmpl(this.model.toJSON()));
    $(this.el).find("#up-instruction-"+ this.model.get("id")).on("click", {that: this}, this.up_instruction);
    $(this.el).find("#delete-instruction-"+ this.model.get("id")).on("click", {that: this}, this.delete_instruction);
    $(this.el).find("#input-"+ this.model.get("id")).on("change", {that: this}, this.content_changed);
    return this;
  },

  events: {
    "change input.content":  "content_changed"
  },

  content_changed: function(event) {
    var that = event.data.that;
    var value = $(that.el).find("#input-"+ that.model.get("id")).val();
    that.model.set("value", value);
  },

  up_instruction: function(event) {
    event.data.that.parent.trigger('up_instruction', event.data.that.model.cid);
  },

  delete_instruction: function(event) {
    event.data.that.parent.trigger('delete_instruction', event.data.that.model.cid);
  }

});