Editor.Collections.Costumes = Backbone.Collection.extend({
  Editor: Editor.Models.Costume,
  localStorage: new Backbone.LocalStorage("costumes")
});