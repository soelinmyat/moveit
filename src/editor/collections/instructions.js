Editor.Collections.Instructions = Backbone.Collection.extend({
  model: Editor.Models.Instruction,
  comparator: function(item) {
    return item.get('serial');
  }
});

Editor.Collections.AvailableInstructions = Editor.Collections.Instructions.extend({
  localStorage: new Backbone.LocalStorage("instructions")
});