Editor.Collections.Backgrounds = Backbone.Collection.extend({
  model: Editor.Models.Background,
  localStorage: new Backbone.LocalStorage("backgrounds")
});